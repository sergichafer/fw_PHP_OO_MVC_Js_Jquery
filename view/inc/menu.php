<body>     
    <div class="fh5co-loader"></div>
    
    <div id="page">
    <nav class="fh5co-nav" role="navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-2">
                    <div id="fh5co-logo"><a href="<?php amigable('?module=main'); ?>">Watupper.</a></div>
                </div>
                <div class="col-md-6 col-xs-6 text-center menu-1">
                    <ul>
                        <li class="<?php if((isset($_GET['module'])) && $_GET['module']=='main'){echo 'active';}else{ echo 'deactivate';}?>">
                          <a href="<?php amigable('?module=main'); ?>">Home</a>
                        </li>
                        <li class="<?php if((isset($_GET['module'])) && $_GET['module']=='products'){echo 'active';}else{ echo 'deactivate';}?>">
                          <a href="<?php amigable('?module=products&view=crud'); ?>">Products</a>
                        </li>
                        <li class="<?php if((isset($_GET['module'])) && $_GET['module']=='services'){echo 'active';}else{ echo 'deactivate';}?>">
                          <a href="<?php amigable('?module=services'); ?>">Services</a>
                        </li>
                        <li class="<?php if((isset($_GET['module'])) && $_GET['module']=='portfolio'){echo 'active';}else{ echo 'deactivate';}?>">
                          <a href="<?php amigable('?module=portfolio'); ?>">Portfolio</a>
                        </li>
                        <li class="<?php if((isset($_GET['module'])) && $_GET['module']=='contact'){echo 'active';}else{ echo 'deactivate';}?>">
                          <a href="<?php amigable('?module=contact&view=view_contact'); ?>">Contact</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-4 text-right hidden-xs menu-2">
                    <ul>
                      <li id="login_menu"></li>

                    </ul>
                </div>
            </div>
            
        </div>
    </nav>
        <?php
          if (!isset($_GET['module'])) {
              echo "<li class='breadcrumb-item'>Home</li>";
          }else if (isset($_GET['module']) && $_GET['module']=='main') {
              echo "";
          } else if (isset($_GET['module']) && !isset($_GET['view'])) {
              echo "<div class='col-md-2'>";
              echo "<nav aria-label='breadcrumb'>";
              echo "<ol class='breadcrumb'>";
              echo "<li class='breadcrumb-item'><a href='../../main/'>Home</a></li>";
              echo "<li class='breadcrumb-item active' aria-current='page'>" . $_GET['module'] . " </li>";
              echo "</ol>";
              echo "</nav>";
              echo "</div>";
          } else {
              echo "<div class='col-md-2'>";
              echo "<nav aria-label='breadcrumb'>";
              echo "<ol class='breadcrumb'>";
              echo "<li class='breadcrumb-item'><a href='../../main/'>Home</a></li>";
              echo "<li class='breadcrumb-item active' aria-current='page'>" . $_GET['module'] . " </li>";
              echo "</ol>";
              echo "</nav>";
              echo "</div>";
          }
        ?>
