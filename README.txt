# Watupper, a FW_OO_MVC_JQuery_Js educational purposes project

## Introduction

This is a basic app that offers login options(register, recover, restore, validations against DAO's, social login alternatives, profile and dependent menus), a listing based on the database made for the app and some contact options for the client.

## Code Samples

To continue with the project up there explained, I'll proceed with the remarkable code inductions.

This project contains code improvements which can be listed here:

-Register validation, functions to acomplish that and proceed with the process keeping in mind a few errors control.

-Login through database option using the JWT generated in the previous register process or social alternatives with the same performance.
    Each user has his own profile page where he can edit personal data and also a personal menu is displayed until the logout button is pressed.

-Website main page is adaptable depending on the most searched products, this ones are displayed in his own page but listed at the main page front for a better relevance, taking care on each user interactions.

-Each product is related to a store and its location is listed depending on the user current location, both in the contact page and product individual page.

There are more code highlights or details but not so remarkable, will be better appreciated at the web app code.

## Installation

> Finally, the way to use the web app is to execute the SQL script found in the DB folder and run the code in an apache server.

## Template site:

TITLE: 
Shop - 100% Fully Responsive Free HTML5 Bootstrap Template

AUTHOR:
DESIGNED & DEVELOPED by FreeHTML5.co

Website: http://freehtml5.co/
Twitter: http://twitter.com/fh5co
Facebook: http://facebook.com/fh5co


CREDITS:

Bootstrap
http://getbootstrap.com/

jQuery
http://jquery.com/

jQuery Easing
http://gsgd.co.uk/sandbox/jquery/easing/

Modernizr
http://modernizr.com/

Google Fonts
https://www.google.com/fonts/

Icomoon
https://icomoon.io/app/

Respond JS
https://github.com/scottjehl/Respond/blob/master/LICENSE-MIT

animate.css
http://daneden.me/animate

jQuery Waypoint
https://github.com/imakewebthings/waypoints/blog/master/licenses.txt

Owl Carousel
http://www.owlcarousel.owlgraphic.com/

Flexslider
https://www.woothemes.com/flexslider/

jQuery countTo
http://www.owlcarousel.owlgraphic.com/

Google Map
https://maps.google.com

Demo Images:
http://unsplash.com

