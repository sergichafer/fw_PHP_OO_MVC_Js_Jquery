CREATE DATABASE  IF NOT EXISTS `watupper` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `watupper`;
-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 28-11-2017 a las 15:13:00
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

DROP TABLE IF EXISTS `tuppers`;

--
-- Estructura de tabla para la tabla `gallery`
--

CREATE TABLE IF NOT EXISTS `tuppers` (
  `reference` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `title` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `inidate` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `finidate` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `interval` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `allergens` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `alimentation` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `specific` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `nutrients` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `details` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `price` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `img` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `store` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Wetaca',
  `relevancy` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

---
--- Dummies for the table watupper
---

INSERT INTO tuppers (`reference`, `title`, `inidate`, `finidate`, `interval`, `allergens`, `alimentation`, `specific`, `nutrients`, `details`, `price`, `img`, `relevancy`) 
VALUES ('a123456', 'Albondigas al pomodoro','22/04/2018', '24/06/2018', '2', 'milk:eggs','Om', 'General Food', 'Carbohydrates','Guisamos lentamente unas jugosas albondigas 100% vacuno en nuestra salsa de tomate artesana. La refrescamos con albahaca y acompanamos de tagliatelle al dente. (Godfather style)', '5.95', '/fw_PHP_OO_MVC_Js_Jquery/view/media/albondigas_pomodoro.jpg', '0'),
('b123456', 'Arroz caldoso de langostinos','22/04/2018', '24/06/2018', '1', 'milk:','Om', 'General Food', 'Carbohydrates','Partiendo de un sofrito de ajo tomate y cebolla cocinamos el mejor arroz redondo en un fumet canero de langostinos y lo dejamos caldoso. Terminamos con aromaticas y un punto citrico.', '5.95', '/fw_PHP_OO_MVC_Js_Jquery/view/media/arroz_caldoso_langostinos.jpg', '0'),
('c123456', 'Crema de setas','22/04/2018', '24/06/2018', '4', 'Crustacean shellfish:Fish','Vt', 'Ovovegetarians', 'Fats or lipids','Cremosa y suave, con un intenso sabor a setas silvestres, redondeada con un pequeno matiz lacteo.', '5.45', '/fw_PHP_OO_MVC_Js_Jquery/view/media/crema_setas.jpg', '0'),
('d123456', 'Lasana de pollo y verduritas','25/04/2018', '29/06/2018', '1', 'Fish:','Om', 'General Food', 'Carbohydrates','Una vuelta de tuerca a las lasanas tipicas. Para el relleno guisamos cuartos traseros de pollo con verduras y setas hasta que estan tiernos, lo desmigamos junto con un poco de caldo de pollo y cubos de pechuga a la parrilla.  Despues gratinamos con nuestra bechamel y un buen punado de Parmiggiano.', '5.95', '/fw_PHP_OO_MVC_Js_Jquery/view/media/lasana_pollo_verduritas.jpg', '0'),
('e123456', 'Menestra de temporada con jamon','22/04/2018', '24/06/2018', '5', 'Milk:','Om', 'General Food', 'Carbohydrates','Seleccionamos las mejores verduras de la temporada, las cocinamos por separado para conseguir el punto perfecto y las integramos con una veloute de su caldo y jamon. Terminamos con daditos de jamon crujiente.', '5.75', '/fw_PHP_OO_MVC_Js_Jquery/view/media/menestra_jamon.jpg', '0'),
('f123456', 'Pollo lacado a la brasa con verduras asadas','22/04/2018', '24/06/2018', '1', 'Soybeans:Milk','Om', 'General Food', 'Proteins','Preparamos una mezcla a base de ketchup casero, soja y jengibre, cocinamos la pechuga de pollo a la brasa de carbon y la lacamos. Acompanamos de boniato para aportar dulzor y untuosidad, calabacin, tomate para reforzar la acidez y unas judias verdes al dente para conseguir un cambio de textura.', '6.25', '/fw_PHP_OO_MVC_Js_Jquery/view/media/pollo_brasa.jpg', '0'),
('k123456', 'Cassoulet de Pato', '14/04/2018', '31/05/2018', '3', 'soybeans:', 'Om', 'General food', 'Proteins', 'Versionamos este tradicional plato de cuchara de la region francesa de Languedoc. ', '6.25', '/fw_PHP_OO_MVC_Js_Jquery/view/media/28170-cassoulet_de_pato.jpg', '0');

---
---
---
DROP TABLE IF EXISTS `stores`;
--
-- Estructura de tabla para la tabla `stores`
--

CREATE TABLE IF NOT EXISTS `stores` (
    store_id varchar(255) COLLATE utf8_spanish_ci NOT NULL,
    storename varchar(255) COLLATE utf8_spanish_ci NOT NULL,
    lat varchar(255) COLLATE utf8_spanish_ci NOT NULL,
    lng varchar(255) COLLATE utf8_spanish_ci NOT NULL,
    PRIMARY KEY (`store_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

---
--- Dummies for the table stores
---

INSERT INTO stores VALUES('000001', 'Wetaca', '40.3361985', '-3.7066831,17'),
('000002', 'Restaurante la Cazuela', '40.3361985', '-3.7066831'),
('000003', 'Gourmet del Socarrat', '38.9884656', '-0.522393'),
('000004', 'Canalla Bistro by Ricard Camarena', '39.4666846', '-0.3774388');

---
---
---
DROP TABLE IF EXISTS `users`;
--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user_id` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `name` varchar(45) COLLATE utf8_spanish_ci,
  `surnames` varchar(100) COLLATE utf8_spanish_ci,
  `identification` varchar(9) COLLATE utf8_spanish_ci,
  `password` varchar(200) COLLATE utf8_spanish_ci,
  `birthdate` varchar(45) COLLATE utf8_spanish_ci,
  `bank` varchar(45) COLLATE utf8_spanish_ci,
  `country` varchar(100) COLLATE utf8_spanish_ci,
  `province` varchar(100) COLLATE utf8_spanish_ci,
  `town` varchar(100) COLLATE utf8_spanish_ci,
  `avatar` varchar(200) COLLATE utf8_spanish_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

---
--- Dummies for the table users
---

INSERT INTO `users` VALUES ('158218454554356','sergislm4@gmail.com','Sergi','Chafer Espi','','Chaferser1','','admin','', ' ',' ','https://robohash.org/set_set3/bgset_bg1/sergislm4@gmail.com.png?size=40x40',1);