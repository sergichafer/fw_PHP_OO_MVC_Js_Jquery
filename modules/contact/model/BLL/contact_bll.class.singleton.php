<?php
class contact_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = contact_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function list_stores_bll() {
        return $this->dao->list_stores_dao($this->db);
    }

}
