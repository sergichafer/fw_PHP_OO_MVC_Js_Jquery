$(document).ready(function () {

    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

	$("#inputBirthdate").datepicker({
        maxDate: '0',
        changeMonth: true,
        changeYear: true,
        yearRange: "1950:2000"
    });


	$('#submitBtn').click(function () {
		register();
    });
	
}); //End of document ready

function register() {

	//regular expressions

	var result = true;
    var emailreg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // var passwordreg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/;
    var passwordreg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    //values

	var email = document.querySelector("input#inputEmail").value;
	var password = document.querySelector("input#inputPassword").value;
	var birthdate = document.querySelector("input#inputBirthdate").value;

	//Validations

	$(".alert").remove();

        //Email

    if ( $("input#inputEmail").val() === "") {
        $("input#inputEmail").focus().after("<span class='alert alert-warning'>Empty Field!</span>");
        result = false;
    } else if (!emailreg.test($("input#inputEmail").val())) {
        $("input#inputEmail").focus().after("<span class='alert alert-warning'>Invalid email!</span>");
        result = false;
    }

        //Passwords

    if ( $("input#inputPassword").val() === "") {
        $("input#inputPassword").focus().after("<span class='alert alert-warning'>Empty field!</span>");
        result = false;
    } else if (!passwordreg.test($("input#inputPassword").val())) {
        $("input#inputPassword").focus().after("<span class='alert alert-warning'>Invalid password!</span>");
        result = false;
    } else if ($("input#inputPassword2").val() === ""){
        $("input#inputPassword2").focus().after("<span class='alert alert-warning'>Empty field!</span>");
        result = false;
    } else if ($("#inputPassword").val() !== $("#inputPassword2").val()) {
        $("#inputPassword").focus().after("<span class='alert alert-warning'>Both password must be equals!</span>");
        $("#inputPassword2").focus().after("<span class='alert alert-warning'>Both password must be equals!</span>");
        result = false;
    }

        //Birthdate

    if ( $("input#inputBirthdate").val() === "") {
        $("input#inputBirthdate").focus().after("<span class='alert alert-warning'>Empty Field!</span>");
        result = false;
    } 

        //Elderly
    var age = calcular_edad(birthdate);
    if (age < 18 || age > 100) {
        $("input#inputBirthdate").focus().after("<span class='alert alert-warning'>Must be a correct age!</span>");
        result = false;
    }

    //Endpoint

    if (result === false) {
        return 0;
    }

    //db validations

    // db_validate("email", email);

    //new user's post
    console.log(result);
    if (result) {

        var data = {"email": email, "password": password, "birthdate": birthdate};
        var data_JSON = JSON.stringify(data);
        $.post(amigable("?module=login&function=register"), {register_json: data_JSON},
            function (response) {

                if(response == 2){
                    $("input#inputEmail").focus().after("<span class='alert alert-danger'>Email already in use!</span>");
                }else{
                    // console.log(response);
                    window.location.href = response;
                }
                // console.log(response);
                
        });
    }
}

function db_validate(field, value){
	$.post(amigable("?module=login&function=db_validate"), {field_to_evaluate: field, value_to_evaluate: value},
    function (data) {
        var message = JSON.parse(data);
    	// console.log(message);
        if (message.length > 0) {
            $("input#inputEmail").focus().after("<span class='alert alert-danger'>Email already in use!</span>");
            return false;
        } else {
            return true;
        }
    });
}

function calcular_edad(fecha){
    var hoy = new Date();
    var cumpleanos = new Date(fecha);
    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
    var m = hoy.getMonth() - cumpleanos.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
        edad--;
    }

    return edad;
}