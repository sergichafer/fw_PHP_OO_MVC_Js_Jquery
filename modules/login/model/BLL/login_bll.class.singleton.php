<?php
class login_bll {
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = login_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function db_validate_BLL($arrArgument){
      return $this->dao->db_validate_DAO($this->db, $arrArgument);
    }

    public function display_menu_BLL($arrArgument){
      return $this->dao->display_menu_DAO($this->db, $arrArgument);
    }

    public function create_user_BLL($arrArgument){
      return $this->dao->create_user_DAO($this->db, $arrArgument);
    }

    public function create_social_BLL($arrArgument){
      return $this->dao->create_social_DAO($this->db, $arrArgument);
    }

    public function update_BLL($arrArgument) {
        return $this->dao->update_DAO($this->db, $arrArgument);
    }

    public function count_BLL($arrArgument) {
        return $this->dao->count_DAO($this->db, $arrArgument);
    }

    public function select_BLL($arrArgument) {
        return $this->dao->select_DAO($this->db, $arrArgument);
    }

    public function obtain_paises_BLL($url) {
        return $this->dao->obtain_paises_DAO($url);
    }

    public function obtain_provincias_BLL() {
        return $this->dao->obtain_provincias_DAO();
    }

    public function obtain_poblaciones_BLL($arrArgument) {
        return $this->dao->obtain_poblaciones_DAO($arrArgument);
    }
}
