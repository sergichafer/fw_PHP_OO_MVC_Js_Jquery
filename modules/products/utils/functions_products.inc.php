<?php
function validate_products($value){

    $error = array();
    $valid = true;
    $filter = array(
        'reference' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/[a-zA-Z]{1}[0-9]{6}$/')
        ),
        'title' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/[a-zA-Z]+([ ][a-zA-Z]){0,2}$/')
        ),
        'price' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(?:0|[1-9]\d*)(?:\.\d{2})?$/')
        ),
        'inidate' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/')
        ),
        'finidate' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/')
        ),
        'details' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(.){1,500}$/')
        ),
    );

    $result = filter_var_array($value, $filter);

    //Non checked data:
    $result['interval'] = $value['interval'];
    $result['allergens'] = $value['allergens'];
    $result['alimentation'] = $value['alimentation'];
    $result['specific'] = $value['specific'];
    $result['nutrients'] = $value['nutrients'];

    if($result['inidate'] && $result['finidate']){
        $dates = validate_dates3($result['inidate'],$result['finidate']);
        if($dates){
            $error['inidate'] = 'Reception date must be before of the expiration date';
            $error['finidate'] = 'Expiration date must be after reception date';
            $valid = false;
        }
    }

    if($result['title']==='Input product name'){
            $error['title']="Input a valid name";
            $valid = false;
    }

    if ($result['alimentation']==='Select alimentation'){
            $error['alimentation']="You need to choose a alimentation";
            $valid = false;
        }

    if ($result['specific']==='Select specific'){
            $error['specific']="You need to choose a specific alimentation";
            $valid = false;
        }

    if ($result['nutrients']==='Select nutrients'){
            $error['nutrients']="You need to choose a nutrients";
            $valid = false;
        }

    if ($result['details']==='Input product description'){
            $error['details']="Please enter a description";
            $valid = false;
    }

    if ($result != null && $result){
        if(!$result['title']){
            $error['title'] = "Input a valid name";
            $valid = false;
        }

        if(!$result['reference']){
            $error['reference'] = "Reference code has to follow the E123456 model";
            $valid = false;
        }

        if(!$result['price']){
            $error['price'] = "Price must be composed by numbers";
            $valid = false;
        }

        if (!$result['inidate']) {
            if ($result['inidate'] == "") {
                $error['inidate'] = "Initial date can't be empty";
                $valid = false;
            } else {
                $error['inidate'] = 'error reception format date (dd/mm/yyyy)';
                $valid = false;
            }
        }

        if (!$result['finidate']) {
            if ($result['finidate'] == "") {
                $error['finidate'] = "Expiration date can't be empty";
                $valid = false;
            } else {
                $error['finidate'] = 'error format date (dd/mm/yyyy)';
                $valid = false;
            }
        }

        if(!$result['details']){
            $error['details'] = "Description must be 2 to 90 letters";
            $valid = false;
        }
    } else {
        $valid = false;
    };

    return $return = array('result' => $valid, 'error' => $error, 'data' => $result );
}//End of function validate product


//http://stackoverflow.com/questions/8722806/how-to-compare-two-dates-in-php

//http://php.net/manual/es/datetime.diff.php

function val_dates($datetime1,$datetime2){
    $date1 = new DateTime();
    $newDate1 = $date1->createFromFormat('d/m/Y', $datetime1);
    $date2 = new DateTime();
    $newDate2 = $date2->createFromFormat('d/m/Y', $datetime2);
    var_dump($newDate1->diff($newDate2));

    if($date1 <= $date2){
      return true;
    }
      return false;
 }

function validate_dates($start_days, $dayslight) {
    $start_day = date("m/d/Y", strtotime($start_days));
    $daylight = date("m/d/Y", strtotime($dayslight));

    list($mes_one, $dia_one, $anio_one) = split('/', $start_day);
    list($mes_two, $dia_two, $anio_two) = split('/', $daylight);

    $dateOne = new DateTime($anio_one . "-" . $mes_one . "-" . $dia_one);
    $dateTwo = new DateTime($anio_two . "-" . $mes_two . "-" . $dia_two);

    if ($dateOne <= $dateTwo) {
        return true;
    }
    return false;
}

function validate_dates2($first_date,$second_date){
      $day1 = substr($first_date, 0, 2);
      $month1 = substr($first_date, 3, 2);
      $year1 = substr($first_date, 6, 4);
      $day2 = substr($second_date, 0, 2);
      $month2 = substr($second_date, 3, 2);
      $year2 = substr($second_date, 6, 4);
      //echo json_encode(strtotime($day2 . "-" . $month2 . "-" . $year2);
      //exit;

      if(strtotime($day1 . "-" . $month1 . "-" . $year1) <= strtotime($day2 . "-" . $month2 . "-" . $year2)){
        return true;
      }
        return false;
}

function validate_dates3($date1,$date2){
  $fixedDate = $date1;
  $variableDate = $date2;
  // Now we do our timestamping magic!
  $fixedDate = implode('', array_reverse(explode('/', $fixedDate)));
  $variableDate = implode('', array_reverse(explode('/', $variableDate)));

    if ($variableDate < $fixedDate){ // 20100428 < 20090501
        return true;
    }
        return false;
}

function check_in_range($start_date, $end_date){
      // Convert to timestamp
      $start_ts = strtotime($start_date);
      $end_ts = strtotime($end_date);

      // Check that user date is between start & end
      if ($start_ts <= $end_ts){
        return true;
      }else{
        return false;
      }
}
