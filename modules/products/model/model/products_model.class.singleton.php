<?php
class products_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = products_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_product($arrArgument) {
        return $this->bll->create_product_BLL($arrArgument);
    }

    public function list_products() {
        return $this->bll->list_products_BLL();
    }

    public function delete_product($criteria) {
        return $this->bll->delete_product_BLL($criteria);
    }

}
