<?php
class products_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_product_DAO($db, $arrArgument) {
        $reference = $arrArgument['reference'];
        $title = $arrArgument['title'];
        $inidate = $arrArgument['inidate'];
        $finidate = $arrArgument['finidate'];
        $interval = $arrArgument['interval'];
        $array = $arrArgument['allergens'];
        foreach ($array as &$value) {
              $allergens=$value.":";
          }
        // @$allergens="";
        // for ($i=0; $i<count($array); $i++){
        //         $allergens = $allergens+$array[$i]+":";  
        //     }
        $alimentation = $arrArgument['alimentation'];
        $specific = $arrArgument['specific'];
        $nutrients = $arrArgument['nutrients'];
        $details = utf8_decode($arrArgument['details']);
        $price = $arrArgument['price'];
        $img = $arrArgument['prodpic'];
        
        $sql = "INSERT INTO `tuppers` (`reference`, `title`, `inidate`, `finidate`,"
                . " `interval`, `allergens`, `alimentation`, `specific`, `nutrients`,"
                . " `details`, `price`, `img`, `relevancy`) VALUES ('$reference', '$title',"
                . " '$inidate', '$finidate', '$interval', '$allergens', "
                . " '$alimentation', '$specific', '$nutrients',"
                . " '$details', '$price', '$img', '0')";

        return $db->ejecutar($sql);
    }
    public function list_products_DAO($db) {
        $sql = "SELECT * FROM tuppers";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function delete_product_DAO($db, $criteria) {
        $sql = "DELETE FROM tuppers WHERE reference LIKE '" . $criteria . "'";
        return $db->ejecutar($sql);
    }

}//End productDAO
