<?php
class products_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = products_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_product_BLL($arrArgument){
      return $this->dao->create_product_DAO($this->db, $arrArgument);
    }

    public function list_products_BLL(){
      return $this->dao->list_products_DAO($this->db);
    }

    public function delete_product_BLL($criteria){
      return $this->dao->delete_product_DAO($this->db, $criteria);
    }

}
