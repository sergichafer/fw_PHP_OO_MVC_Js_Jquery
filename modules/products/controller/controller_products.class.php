<?php
class controller_products {
    function __construct() {
        include(UTILS_PRODUCTS . "functions_products.inc.php");
        include(UTILS . "upload.inc.php");
        $_SESSION['module'] = "products";
    }

  function crud() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/products/view/', 'crud.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

  function delete_product() {
        if (isset($_POST)) {
          $_POST = json_decode(file_get_contents('php://input'), true);
          $product = $_POST["param"];
          
          $rdo = loadModel(MODEL_PRODUCTS,"products_model", "delete_product", $product);
          // echo json_encode($rdo);
          // exit();
        }
    }

    function list_products() {
      $rdo = loadModel(MODEL_PRODUCTS,"products_model", "list_products");
      $list = array();
      foreach ($rdo as $row) {
          array_push($list, $row);
      }
      echo json_encode($list);
      exit();     
    }

    function create_products() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/products/view/', 'create_products.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function results_products() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/products/view/', 'results_products.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function upload_prodpic() {
        if ((isset($_POST["upload"])) && ($_POST["upload"] == true)) {
            $result_prodpic = upload_files();
            $_SESSION['result_prodpic'] = $result_prodpic;
            echo debugPHP($_SESSION['result_avatar']); // show it   in alert(response) dropzone
        }
    }

    function alta_products(){
      if ((isset($_POST['alta_products_json']))) {
        $jsondata = array();
        $producstJSON = json_decode($_POST["alta_products_json"], true);
        $result= validate_products($producstJSON);
        // echo json_encode($result);
        // exit();
        if (empty($_SESSION['result_prodpic'])){
            $_SESSION['result_prodpic'] = array('result' => true, 'error' => "", "data" => "./media/default-avatar.png");
        }
        $result_prodpic = $_SESSION['result_prodpic'];
        // echo json_encode($result_prodpic);
        // exit();
        if(($result['result']) && ($result_prodpic['result'])) {
            $arrArgument = array(
              'reference' => $result['data']['reference'],
              'title' => $result['data']['title'],
              'inidate' => $result['data']['inidate'],
              'finidate' => $result['data']['finidate'],
              'interval' => $result['data']['interval'],
              'allergens' => $result['data']['allergens'],
              'alimentation' => $result['data']['alimentation'],
              'specific' => $result['data']['specific'],
              'nutrients' => $result['data']['nutrients'],
              'details' => $result['data']['details'],
              'price' => $result['data']['price'],
              'prodpic' => $result_prodpic['data']
            );
            //$arrValue=true;
            // echo json_encode($arrArgument);
            // exit();
            $arrValue = false;
            $arrValue = loadModel(MODEL_PRODUCTS, "products_model", "create_product", $arrArgument);
            // echo json_encode($arrValue);
            // exit();

            if ($arrValue){
                $message = "Product has been successfull registered";
            }else{
                $message = "Problem ocurred registering a product";
            }

            $_SESSION['product'] = $arrArgument;
            $_SESSION['message'] = $message;
            $callback="../../products/results_products/";

            $jsondata['success'] = true;
            $jsondata['redirect'] = $callback;
            echo json_encode($jsondata);
            exit;
        }else{
          $jsondata['success'] = false;
          $jsondata['error'] = $result['error'];
          $jsondata['error_prodpic'] = $result_prodpic['error'];

          $jsondata['success1'] = false;
          if ($result_prodpic['result']) {
              $jsondata['success1'] = true;
              $jsondata['prodpic'] = $result_prodpic['data'];
          }
          header('HTTP/1.0 400 Bad error');
          echo json_encode($jsondata);
        }//End else
      }//End alta products
    }

    function delete_prodpic() {
        if (isset($_POST["delete"]) && $_POST["delete"] == true) {
            $_SESSION['result_avatar'] = array();
            $result = remove_files();
            if ($result === true) {
                echo json_encode(array("res" => true));
            } else {
                echo json_encode(array("res" => false));
            }
        }
    }

    function load_products() {
      if (isset($_POST["load"]) && $_POST["load"] == true) {
        $jsondata = array();
        if (isset($_SESSION['product'])) {
            //echo debug($_SESSION['user']);
            $jsondata["product"] = $_SESSION['product'];
        }
        if (isset($_SESSION['message'])) {
            //echo $_SESSION['msje'];
            $jsondata["message"] = $_SESSION['message'];
        }
        close_session();
        echo json_encode($jsondata);
        exit;
      }
    }

    function load_data_products() {
      if ((isset($_GET["load_data"])) && ($_GET["load_data"] == true)) {
        $jsondata = array();

        if (isset($_SESSION['product'])) {
            $jsondata["product"] = $_SESSION['product'];
            echo json_encode($jsondata);
            exit;
        } else {
            $jsondata["product"] = "";
            echo json_encode($jsondata);
            exit;
        }
      }
    }

    function close_session() {
        unset($_SESSION['product']);
        unset($_SESSION['message']);
        $_SESSION = array(); // Destruye todas las variables de la sesión
        session_destroy(); // Destruye la sesión
    }

}

