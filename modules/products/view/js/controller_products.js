//Plugin to put the values into the fields if them are empty
jQuery.fn.fill_or_clean = function () {
    this.each(function () {
        if ($("#reference").val() === "") {
            $("#reference").val("Input product reference");
            $("#reference").focus(function () {
                if ($("#reference").val() === "Input product reference") {
                    $("#reference").val("");
                }
            });
        }
        $("#reference").blur(function () { //Onblur is activated when user changes the focus
            if ($("#reference").val() === "") {
                $("#reference").val("Input product reference");
            }
        });//Product reference end

        if ($("#title").val() === "") {
            $("#title").val("Input product title");
            $("#title").focus(function () {
                if ($("#title").val() === "Input product title") {
                    $("#title").val("");
                }
            });
        }
        $("#title").blur(function () { //Onblur is activated when user changes the focus
            if ($("#title").val() === "") {
                $("#title").val("Input product title");
            }
        });//Product title end

        if ($("#inidate").val() === "") {
            $("#inidate").val("Input initial date");
            $("#inidate").focus(function () {
                if ($("#inidate").val() === "Input initial date") {
                    $("#inidate").val("");
                }
            });
        }
        $("#inidate").blur(function () { //Onblur is activated when user changes the focus
            if ($("#inidate").val() === "") {
                $("#inidate").val("Input initial date");
            }
        });//Initial date end

        if ($("#finidate").val() === "") {
            $("#finidate").val("Input expiration date");
            $("#finidate").focus(function () {
                if ($("#finidate").val() === "Input expiration date") {
                    $("#finidate").val("");
                }
            });
        }
        $("#finidate").blur(function () { //Onblur is activated when user changes the focus
            if ($("#finidate").val() === "") {
                $("#finidate").val("Input expiration date");
            }
        });//Expiration date end

        if ($("#details").val() === "") {
            //console.log("Inside first if details");
            $("#details").val("Input product details");
            $("#details").focus(function () {
                if ($("#details").val() === "Input product details") {
                  //console.log("Inside second if details");
                    $("#details").val("");
                }
            });
        }
        $("#details").blur(function () { //Onblur is activated when user changes the focus
            if ($("#details").val() === "") {
                $("#details").val("Input product details");
            }
        });//Product details end

        if ($("#price").val() === "") {
            $("#price").val("Input product price");
            $("#price").focus(function () {
                if ($("#price").val() === "Input product price") {
                    $("#price").val("");
                }
            });
        }
        $("#price").blur(function () { //Onblur is activated when user changes the focus
            if ($("#price").val() === "") {
                $("#price").val("Input product price");
            }
        });//Product price end

    });//End of the each
    return this;
};// End of fill or clean function

//Solution to : "Uncaught Error: Dropzone already attached."
Dropzone.autoDiscover = false;
$(document).ready(function () {
    //console.log("Inside ready");

    $( "#inidate" ).datepicker({
        dateFormat: 'dd/mm/yy',
        //dateFormat: 'mm-dd-yy',
        changeMonth: true, changeYear: true,
        minDate: -90, maxDate: "+1M"
    });
    $( "#finidate" ).datepicker({
      dateFormat: 'dd/mm/yy',
      //dateFormat: 'mm-dd-yy',
      changeMonth: true, changeYear: true,
      minDate: 0, maxDate: "+36M"
    });


    $('#submit_products').click(function(){
        //console.log("Inside click function");
        //console.log($('input[name="packaging"]:checked').val());
        validate_product();
    });

    $.post(amigable("?module=products&function=load_data_products"),{'load_data':true},
          function(response){
            if(response.product===""){
                $("#reference").val('');
                $("#title").val('');
                $("#inidate").val('');
                $("#finidate").val('');
                var inputElements = document.getElementsByClassName('allergens');
                for (var i = 0; i < inputElements.length; i++) {
                    if (inputElements[i].checked){
                        inputElements[i].checked = false;
                    }
                }
                $('#alimentation').val('Select alimentation');
                $('#specific').val('Select specific');
                $('#nutrients').val('Select nutrients');
                $("#details").val('');
                $("#price").val('');
                
            $(this).fill_or_clean();
            }else{
              $("#reference").val(response.product.reference);
              $("#title").val(response.product.title);
              $("#inidate").val(response.product.inidate);
              $("#finidate").val(response.product.finidate);
              var allergens = response.product.allergens;
              var inputElements = document.getElementsByClassName('allergens');
              for (var j = 0; j < allergens.length; j++) {
                  for (var k = 0; k < inputElements.length; k++) {
                    if (allergens[j] === inputElements[k]){
                        inputElements[k].checked = true;
                    }
                  }
              }
              $('#alimentation').val(response.product.alimentation);
              $('#specific').val(response.product.specific);
              $('#nutrients').val(response.product.nutrients);
              $("#details").val(response.product.details);
              $("#price").val(response.product.price);
            }
          }, "json");

    //Dropzone function //////////////////////////////////
    $("#dropzone").dropzone({
        url: amigable("?module=products&function=upload_prodpic"),
        params:{'upload':true},
        addRemoveLinks: true,
        maxFileSize: 1000,
        dictResponseError: "An error has occurred on the server",
        acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function () {
            this.on("success", function (file, response) {
                //alert(response);
                $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
                console.log(file.name);
                console.log("Response: "+response);
            });
        },
        complete: function (file) {
            //if(file.status == "success"){
            //alert("El archivo se ha subido correctamente: " + file.name);
            //}
        },
        error: function (file) {
            //alert("Error subiendo el archivo " + file.name);
        },
        removedfile: function (file, serverFileName) {
            var name = file.name;
            console.log(name);
            $.ajax({
                type: "POST",
                url: amigable("?module=products&function=delete_prodpic"),
                data: {"filename":name,"delete":true},
                success: function (data) {
                  //console.log(name);
                  console.log(data);
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");

                    var json = JSON.parse(data);
                    //console.log(data);
                    if (json.res === true) {
                        var element;
                        if ((element = file.previewElement) !== null) {
                            element.parentNode.removeChild(file.previewElement);
                            //alert("Imagen eliminada: " + name);
                        } else {
                            return false;
                        }
                    } else { //json.res == false, elimino la imagen también
                        var element2;
                        if ((element2 = file.previewElement) !== null) {
                            element2.parentNode.removeChild(file.previewElement);
                        } else {
                            return false;
                        }
                    }

                }
            });
        }
    });//End dropzone

    var val_dates = /\d{2}.\d{2}.\d{4}$/;
    var ref_reg = /[a-zA-Z]{1}[0-9]{6}$/;
    var title_reg = /[a-zA-Z]+([ ][a-zA-Z]){0,2}$/;
    var price_reg = /^(?:0|[1-9]\d*)(?:\.\d{2})?$/;
    var string_details = /^(.){1,500}$/;
    //var string_description = /^[0-9A-Za-z]{2,90}$/;

    /* Fade out function  to hide the error messages */

    $("#reference").keyup(function () {
      if ($(this).val() !== "" && ref_reg.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#title").keyup(function () {
      if ($(this).val() !== "" && title_reg.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#price").keyup(function () {
      if ($(this).val() !== "" && price_reg.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#details").keyup(function () {
      if ($(this).val() !== "" && string_details.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    //Dependent combos //////////////////////////////////

    $.getJSON("../../resources/alimentation.json", function (data) {
        alimentation = data;
        // console.log(alimentation);
        setTimeout(function () {
            if (alimentation !== undefined) {
                load_alimentation();
            }
        }, 2000);
    });

    $.getJSON("../../resources/specific.json", function (data) {
        specific = data;
        setTimeout(function () {
            if (alimentation !== undefined) {
                load_specific();
            }
        }, 2000);
    });

    $.getJSON("../../resources/nutrition.json", function (data) {
        nutrition = data;
        setTimeout(function () {
            if (specific !== undefined) {
                load_nutrients();
            }
        }, 2000);
    });

    $("#alimentation").change(function () {
        var Code1 = $("#alimentation").val();
        load_specific(Code1);
    });

    $("#specific").change(function () {
        var specific = $("#specific").val();
        load_nutrients(specific);
    });
    
    
});//End document ready

function validate_product(){
    var result = true;

    var reference = document.getElementById('reference').value;
    var title = document.getElementById('title').value;
    var inidate = document.getElementById('inidate').value;
    var finidate = document.getElementById('finidate').value;
    var interval = $('input[name=interval]:checked').val();
    var allergens = [];
    var inputElements = document.getElementsByClassName('allergens');
    var j=0;
    for (var i=0; i< inputElements.length; i++){
        if (inputElements[i].checked){
          allergens[j] = inputElements[i].value;
          j++;
        }
    }
  /*  var packaging = [];
    var radio = document.getElementsByClassName('packaging');
    var k=0;
    for (var l=0; l<radio.length; l++){
        if (radio[l].checked){
          packaging[k] = radio[l].value;
          k++;
        }
    }*/

/*
    var c = document.getElementById('country');
    var country = c.options[c.selectedIndex].text;
    var p = document.getElementById('province');
    var province = p.options[p.selectedIndex].text;
*/
    var alimentation = document.getElementById('alimentation').value;
    var specific = document.getElementById('specific').value;
    var nutrients = document.getElementById('nutrients').value;
    var details = document.getElementById('details').value;
    var price = document.getElementById('price').value;

    var val_dates = /\d{2}.\d{2}.\d{4}$/;
    var ref_reg = /[a-zA-Z]{1}[0-9]{6}$/;
    var title_reg = /[a-zA-Z]+([ ][a-zA-Z]){0,2}$/;
    var price_reg = /^(?:0|[1-9]\d*)(?:\.\d{2})?$/;
    var string_details = /^(.){1,500}$/;

    $(".error").remove();

    if ($("#reference").val() === "" || $("#reference").val() === "Input product reference") {
        $("#reference").focus().after("<span class='error'>Input product reference</span>");
        return false;
    } else if (!ref_reg.test($("#reference").val())) {
        $("#reference").focus().after("<span class='error'>Reference must follow the scheme A012345</span>");
        return false;
    }

    if ($("#title").val() === "" || $("#title").val() === "Input product name"){
      $("#title").focus().after("<span class='error'>Input product name</span>");
      return false;
    }else if(!title_reg.test($("#title").val())){
      $("#title").focus().after("<span class='error'>Name must be valid</span>");
      return false;
    }

    if ($("#inidate").val() === "" || $("#inidate").val() === "Input reception date") {
        $("#inidate").focus().after("<span class='error'>JS Input product reception date</span>");
        return false;
    } else if (!val_dates.test($("#inidate").val())) {
        $("#inidate").focus().after("<span class='error'>JS Input product reception date</span>");
        return false;
    }

    if ($("#finidate").val() === "" || $("#finidate").val() === "Input expiration date") {
        $("#finidate").focus().after("<span class='error'>JS Input product expiration date</span>");
        return false;
    } else if (!val_dates.test($("#finidate").val())) {
        $("#finidate").focus().after("<span class='error'>JS Input product expiration date</span>");
        return false;
    }

    if ($("#alimentation").val() === "" || $("#alimentation").val() === "Select alimentation" || $("#alimentation").val() === null) {
        $("#alimentation").focus().after("<span class='error'>Select one type</span>");
        return false;
    }

    if ($("#specific").val() === "" || $("#specific").val() === "Select specific") {
        $("#specific").focus().after("<span class='error'>Select one specific type</span>");
        return false;
    }

    if ($("#nutrients").val() === "" || $("#nutrients").val() === "Select nutrients") {
        $("#nutrients").focus().after("<span class='error'>Select one nutrient to priorize</span>");
        return false;
    }

    if ($("#details").val() === "" || $("#details").val() === "Input product details") {
        $("#details").focus().after("<span class='error'>Input product details</span>");
        return false;
    } else if (!string_details.test($("#details").val())) {
        $("#details").focus().after("<span class='error'>Details cannot be empty</span>");
        return false;
    }

    if ($("#price").val() === "" || $("#price").val() === "Input product price") {
        $("#price").focus().after("<span class='error'>Input product price</span>");
        return false;
    } else if (!price_reg.test($("#price").val())) {
        $("#price").focus().after("<span class='error'>Price must be numbers (like 1.2)</span>");
        return false;
    }

    // console.log("Before if result");
    if (result){
      // console.log("Inside if result");

        if (specific === null) {
            specific = 'default_specific';
        }else if (specific.length === 0) {
            specific = 'default_specific';
        }else if (specific === 'Select specific') {
            return 'default_specific';
        }

        if (nutrients === null) {
            nutrients = 'default_nutrients';
        }else if (nutrients.length === 0) {
            nutrients = 'default_nutrients';
        }else if (nutrients === 'Select nutrients') {
            return 'default_nutrients';
        }

      var data = {"reference": reference, "title": title, "inidate": inidate, "finidate": finidate, "interval": interval, "allergens": allergens,"alimentation": alimentation,"specific": specific, "nutrients": nutrients, "details": details, "price": price};
      console.log(data); //Apleguen les dades be
      var data_products_JSON = JSON.stringify(data);
      var post_url = amigable("?module=products&function=alta_products");
      $.post(post_url,{alta_products_json:data_products_JSON}, function (response){
        console.log(response);//Aqui muestra los resultados de PHP
        //console.log(response.prodname);
        if(response.success){
          window.location.href = response.redirect;
        }
    },"json").fail(function(xhr, textStatus, errorThrown){
          console.log("Inside error json");
          console.log(xhr);
          if (xhr.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (xhr.status == 404) {
                alert('Requested page not found [404]');
            } else if (xhr.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + xhr.responseText);
            }
          if (xhr.responseJSON == 'undefined' && xhr.responseJSON === null )
                  xhr.responseJSON = JSON.parse(xhr.responseText);

          if(xhr.responseJSON.error.reference)
            $("#error_reference").focus().after("<span  class='error1'>" + xhr.responseJSON.error.reference + "</span>");

          if(xhr.responseJSON.error.title)
            $("#error_title").focus().after("<span  class='error1'>" + xhr.responseJSON.error.title + "</span>");

          if(xhr.responseJSON.error.inidate)
            $("#error_inidate").focus().after("<span  class='error1'>" + xhr.responseJSON.error.inidate + "</span>");

          if(xhr.responseJSON.error.finidate)
            $("#error_finidate").focus().after("<span  class='error1'>" + xhr.responseJSON.error.finidate + "</span>");

          if(xhr.responseJSON.error.allergens)
            $("#error_allergens").focus().after("<span  class='error1'>" + xhr.responseJSON.error.allergens + "</span>");

          if(xhr.responseJSON.error.alimentation)
            $("#error_alimentation").focus().after("<span  class='error1'>" + xhr.responseJSON.error.alimentation + "</span>");

          if(xhr.responseJSON.error.specific)
            $("#error_specific").focus().after("<span  class='error1'>" + xhr.responseJSON.error.specific + "</span>");

          if(xhr.responseJSON.error.nutrients)
            $("#error_nutrients").focus().after("<span  class='error1'>" + xhr.responseJSON.error.nutrients + "</span>");

          if(xhr.responseJSON.error.details)
            $("#error_details").focus().after("<span  class='error1'>" + xhr.responseJSON.error.details + "</span>");

          if(xhr.responseJSON.error.price)
            $("#error_price").focus().after("<span  class='error1'>" + xhr.responseJSON.error.price + "</span>");

          if(xhr.responseJSON.error_avatar)
            $("#prodpic").focus().after("<span  class='error1'>" + xhr.responseJSON.error_avatar + "</span>");

          if (xhr.responseJSON.success1) {
                if (xhr.responseJSON.img_avatar !== "./view/media/default-avatar.png") {
                    //$("#progress").show();
                    //$("#bar").width('100%');
                    //$("#percent").html('100%');
                    //$('.msg').text('').removeClass('msg_error');
                    //$('.msg').text('Success Upload image!!').addClass('msg_ok').animate({ 'right' : '300px' }, 300);
                }
            } else {
                $("#progress").hide();
                $('.msg').text('').removeClass('msg_ok');
                $('.msg').text('Error Upload image!!').addClass('msg_error').animate({'right': '300px'}, 300);
            }

    });//End fail function hrx
  }//End if result
}//End validate_product

var searchIntoJson = function (obj, column, value) {
    var results = [];
    var valueField;
    var searchField = column;
    // console.log(column);
    for (var i = 0 ; i < obj.length ; i++) {
        valueField = obj[i][searchField].toString();
        if (valueField === value) {
            results.push(obj[i]);
        }
    }
    return results;
};
var alimentation;
var specific;
var nutrition;


var load_alimentation = function () {
    $("#alimentation").empty();
    $("#alimentation").append('<option  value="" selected="selected"></option>');
    $.each(alimentation, function (i, valor) {
        // alert(valor);
        $("#alimentation").append("<option class='example' id=" + valor.Type + " value='" + valor.Code + "'>" + valor.Type + "</option>");
    });
};

var load_specific = function (Code1) {
    var specificCode = searchIntoJson(specific, "Code", Code1);
    $("#specific").empty();
    $("#specific").append('<option value="" selected="selected"></option>');
    $.each(specificCode, function (i, valor) {
        $("#specific").append('<option value="' + valor.Alimentation + '">' + valor.Alimentation + '</option>');
    });
};

var load_nutrients = function (specific) {
    var specificAlimentation = searchIntoJson(nutrition, "Alimentation", specific);
    $("#nutrients").empty();
    $("#nutrients").append('<option value="" selected="selected"></option>');
    $.each(specificAlimentation, function (i, valor) {
        $("#nutrients").append('<option value="' + valor.Nutrition + '">' + valor.Nutrition + '</option>');
    });
};

