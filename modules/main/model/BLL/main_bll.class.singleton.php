<?php
class main_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = main_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function list_products_bll() {
        return $this->dao->list_products_dao($this->db);
    }

    public function page_products_bll($arrArgument) {
        return $this->dao->page_products_dao($this->db, $arrArgument);
    }

    public function total_products_bll($criteria) {
        return $this->dao->total_products_dao($this->db, $criteria);
    }

    public function obtain_product_bll($criteria) {
        return $this->dao->obtain_product_dao($this->db, $criteria);
    }

    public function live_search_bll() {
        return $this->dao->live_search_dao($this->db);
    }

    public function increment_bll($criteria) {
        return $this->dao->increment_dao($this->db, $criteria);
    }

    public function total_rows_like_bll($criteria) {
        return $this->dao->total_rows_like_dao($this->db, $criteria);
    }

    public function load_more_bll($criteria) {
        return $this->dao->load_more_dao($this->db, $criteria);
    }

    public function list_store_bll($criteria) {
        return $this->dao->list_store_dao($this->db, $criteria);
    }
}
