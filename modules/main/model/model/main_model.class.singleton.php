<?php
class main_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = main_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function main_model(){
        return $this->bll->list_products_bll();
    }

    public function live_search() {
        return $this->bll->live_search_bll();
    }

    public function increment($criteria) {
        return $this->bll->increment_bll($criteria);
    }

    public function page_products($arrArgument) {
        return $this->bll->page_products_bll($arrArgument);
    }

    public function total_products($criteria) {
        return $this->bll->total_products_bll($criteria);
    }

    public function obtain_product($criteria) {
        return $this->bll->obtain_product_bll($criteria);
    }

    public function total_rows_like($criteria) {
        return $this->bll->total_rows_like_bll($criteria);
    }

    public function load_more($criteria) {
        return $this->bll->load_more_bll($criteria);
    }

    public function list_store($criteria) {
        return $this->bll->list_store_bll($criteria);
    }
}
