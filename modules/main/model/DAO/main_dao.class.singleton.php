<?php
class main_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function list_products_dao($db) {
        // $servername = "127.0.0.1";
        // $username = "root";
        // $password = "";
        // $dbname = "watupper";

        // // Create connection
        // $conn = new mysqli($servername, $username, $password, $dbname);
        // // Check connection
        // if ($conn->connect_error) {
        //    die("Connection failed: " . $conn->connect_error);
        // }

        // $sql = "SELECT * FROM tuppers";
        // $result = $conn->query($sql);
        // $data = array();
        // if ($result->num_rows > 0) {
        //    while ($row = $result->fetch_assoc()) {
        //     array push($data, $row["reference"]);          
        //     }
        //     return $data;
        // } else {
        //    return "0 results";
        // }
        // $conn->close();
        $sql = "SELECT * FROM tuppers";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function live_search_dao($db) {
        $sql = "SELECT title FROM tuppers ORDER BY title";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function increment_dao($db, $criteria) {
        $sql = "UPDATE tuppers SET relevancy=relevancy+1 WHERE title LIKE '" . $criteria . "%'";
        return $db->ejecutar($sql);
    }

    public function page_products_dao($db, $arrArgument) {
        $position = $arrArgument['position'];
        $item_per_page = $arrArgument['item_per_page'];
        $criteria = $arrArgument['criteria'];

        $sql = "SELECT DISTINCT * FROM tuppers WHERE title like '" . $criteria . "%' ORDER BY title ASC LIMIT " . $position . ", " . $item_per_page;

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function total_products_dao($db, $criteria) {
        $sql = "SELECT COUNT(*) as total FROM tuppers WHERE title like '%" . $criteria . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function obtain_product_dao($db, $criteria) {
        $sql = "SELECT DISTINCT * FROM tuppers WHERE title like '" . $criteria . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }


    public function total_rows_like_dao($db, $criteria) {
        $sql = "SELECT COUNT(*) as total FROM tuppers WHERE title like '%" . $criteria . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function load_more_dao($db, $criteria) {
        $sql = "SELECT * FROM tuppers ORDER BY relevancy DESC LIMIT " . $criteria;
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function list_store_dao($db, $criteria) {
        $sql = "SELECT * FROM stores WHERE storename LIKE '" . $criteria . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

}