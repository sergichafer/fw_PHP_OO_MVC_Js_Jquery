<?php

//SITE_ROOT

$path = $_SERVER['DOCUMENT_ROOT'] . '/fw_PHP_OO_MVC_Js_Jquery/';
define('SITE_ROOT', $path);

//SITE_PATH

define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/fw_PHP_OO_MVC_Js_Jquery/');

//CSS

define('CSS_PATH', SITE_PATH . 'view/css/');

//JS

define('JS_PATH', SITE_PATH . 'view/js/');

//IMG

define('media_PATH', SITE_PATH . 'view/media/');
define('IMG_PATH', SITE_PATH . 'view/images/');


// //log
// define('USER_LOG_DIR', SITE_ROOT . 'log/user/Site_User_errors.log');
// define('GENERAL_LOG_DIR', SITE_ROOT . 'log/general/Site_General_errors.log');

define('PRODUCTION', true);

//model

define('MODEL_PATH', SITE_ROOT . 'model/');

//view

define('VIEW_PATH_INC', SITE_ROOT . 'view/inc/');
define('VIEW_PATH_INC_ERROR', SITE_ROOT . 'view/inc/templates_error/');

//modules

define('MODULES_PATH', SITE_ROOT . 'modules/');

//resources

define('RESOURCES', SITE_ROOT . 'resources/');

//media

define('MEDIA_PATH', SITE_ROOT . 'view/media/');

//utils

define('UTILS', SITE_ROOT . 'utils/');

//libs

define('LIBS', SITE_ROOT . '/libs/');

//model main

define('FUNCTIONS_MAIN', SITE_ROOT . 'modules/main/utils/');
define('MODEL_PATH_MAIN', SITE_ROOT . 'modules/main/model/');
define('DAO_MAIN', SITE_ROOT . 'modules/main/model/DAO/');
define('BLL_MAIN', SITE_ROOT . 'modules/main/model/BLL/');
define('MODEL_MAIN', SITE_ROOT . 'modules/main/model/model/');
define('MAIN_JS_PATH', SITE_PATH . 'modules/main/view/js/');

//model login


define('LOGIN_JS_PATH', SITE_PATH . 'modules/login/view/js/');
define('UTILS_LOGIN', SITE_ROOT . 'modules/login/utils/');
define('LOGIN_VIEW_PATH', 'modules/login/view/');
define('MODEL_PATH_LOGIN', SITE_ROOT . 'modules/login/model/');
define('DAO_LOGIN', SITE_ROOT . 'modules/login/model/DAO/');
define('BLL_LOGIN', SITE_ROOT . 'modules/login/model/BLL/');
define('MODEL_LOGIN', SITE_ROOT . 'modules/login/model/model/');


//model products

define('UTILS_PRODUCTS', SITE_ROOT . 'modules/products/utils/');
// define('PRODUCTS_JS_LIB_PATH', SITE_PATH . 'modules/products/view/lib/');
define('PRODUCTS_JS_PATH', SITE_PATH . 'modules/products/view/js/');
define('MODEL_PATH_PRODUCTS', SITE_ROOT . 'modules/products/model/');
define('DAO_PRODUCTS', SITE_ROOT . 'modules/products/model/DAO/');
define('BLL_PRODUCTS', SITE_ROOT . 'modules/products/model/BLL/');
define('MODEL_PRODUCTS', SITE_ROOT . 'modules/products/model/model/');

//model contact

define('CONTACT_JS_PATH', SITE_PATH . 'modules/contact/view/js/');
define('CONTACT_CSS_PATH', SITE_PATH . 'modules/contact/view/css/');
define('CONTACT_LIB_PATH', SITE_PATH . 'modules/contact/view/lib/');
define('CONTACT_IMG_PATH', SITE_PATH . 'modules/contact/view/img/'); 
define('CONTACT_VIEW_PATH', 'modules/contact/view/');
define('DAO_CONTACT', SITE_ROOT . 'modules/contact/model/DAO/');
define('BLL_CONTACT', SITE_ROOT . 'modules/contact/model/BLL/');
define('MODEL_CONTACT', SITE_ROOT . 'modules/contact/model/model/');

//amigables

define('URL_AMIGABLES', TRUE);
